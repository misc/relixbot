#    RElixBot, a simple Matrix relay bot
#    Based on srIRCeBot, a Simple IRC RElay Bot
#
#    Copyright (C) 2022 Ferass EL HAFIDI <vitali64pmemail@protonmail.com>
#    Copyright (C) 2022 Andrew Yu <andrew@andrewyu.org>
#
#    Modifications are:
#    Copyright (C) 2022 Ferass EL HAFIDI <vitali64pmemail@protonmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import miniirc
import miniirc_matrix
from miniirc_extras import *
from time import sleep

# Configuration

server          = [ ['irc.libera.chat', 'libera'], ['mtrx.vern.cc', 'matrix'] ]
# Use the internal matrix room address.
relayedChannels = [ [ '#chan1', '!aBCrdDeRfG:matrix.org' ], [ '##chan2', '!xgwAUWHbwWUG:vern.cc' ] ]
nick            = 'TestRelay'
debug           = True
channels        = [ 0, 1 ] # Multiple channels
join_leave_msgs = False # Relay Join/Left messages.

# IRC
irc = miniirc.IRC(server[0][0], 6697, nick, (relayedChannels[i][0] for i in channels), 
    ns_identity=None, debug=True)
# Matrix
matrix = miniirc_matrix.Matrix(server[1][0], 443, nick, 
    None, debug=True, token=miniirc_matrix.login("", "", ""))

for i in channels:
    matrix.send('JOIN', relayedChannels[i][1])

# Colours
def colours(string):
    colour = [ '03', '04', '06', '07', '08', '09', '10' ]
    return colour[hash(string.encode()) % \
        len(colour)]

# Relay functions

def relay_topic(chat, hostmask, args, which):
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    # Sync topic/description
    chat.send('TOPIC', relayedChannels[currentchan][whichchan], 
                args[-1])

def relay_msgs(chat, hostmask, args, which):
    #sleep(0.5) # Sleep for 0.5 seconds
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    # Send in the irc side what the user sent in the matrix side and vice-versa
    if args[-1].startswith("ACTION"):
        words = args[-1].replace("", "").replace("ACTION", "")
        chat.me(relayedChannels[currentchan][whichchan], 
                    " \x03%s%s\x03 %s" 
                    % (colours(hostmask[0]), hostmask[0], words))
    else:
        chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "<\x03%s%s\x03> %s" 
                % (colours(hostmask[0]), hostmask[0], args[-1]))

def relay_nick(chat, hostmask, args, which):
    #sleep(0.5)
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    # Send in the matrix side the nick change in the irc side
    chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "\x03%s%s\x03 is now known as \x0302%s" 
                % (colours(hostmask[0]), hostmask[0], args[-1]))

def relay_joins(chat, hostmask, args, which):
    #sleep(0.5)
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    if hostmask[0] == nick: return
    # Send in the matrix side joins in the irc side and vice-versa
    chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "\x03%s%s\x03 has joined \x02%s" 
                % (colours(hostmask[0]), hostmask[0], args[-1]))

def relay_mode(chat, hostmask, args, which):
    #sleep(0.5)
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "mode/\x02%s\x02: [%s %s] by \x03%s%s" 
                % (args[0], args[1], args[2], colours(hostmask[0]), hostmask[0]))

def relay_quits(chat, hostmask, args, which):
    #sleep(0.5)
    whichchan = 0 if which == 1 else 1
    currentchan = None
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    if hostmask[0] == nick: return
    # Send in the matrix side quits in the irc side and vice-versa
    chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "\x03%s%s\x03 has left \x02%s" 
                % (colours(hostmask[0]), hostmask[0], args[0]))

def relay_kicks(chat, hostmask, args, which):
    #sleep(0.5)
    currentchan = None
    whichchan = 0 if which == 1 else 1
    for i in channels:
        if args[0] == relayedChannels[i][which]:
            currentchan = i
    if currentchan == None: return
    # Send in the matrix side kicks in the irc side and vice-versa
    chat.send('PRIVMSG', relayedChannels[currentchan][whichchan], 
                "\x03%s%s\x03 was kicked from %s by %s" 
                % (colours(args[1]), args[1], args[0], hostmask[0]))

# Functions that run the relay functions, depending on the server where the message has been sent
@irc.Handler('NICK', colon=False)
def handle_nicks(chat, hostmask, args):
    if chat == irc:
        relay_nick(matrix, hostmask, args, 0)
    elif chat == matrix:
        relay_nick(irc, hostmask, args, 1)


@irc.Handler('KICK', colon=False)
def handle_kicks(chat, hostmask, args):
    if chat == irc:
        relay_kicks(matrix, hostmask, args, 0)
    elif chat == matrix:
        relay_kicks(irc, hostmask, args, 1)


@miniirc.Handler('JOIN', colon=False)
def handle_joins(chat, hostmask, args):
    if join_leave_msgs == False: return
    if chat == irc:
        relay_joins(matrix, hostmask, args, 0)
    elif chat == matrix:
        relay_joins(irc, hostmask, args, 1)

@miniirc.Handler('PART', colon=False)
def handle_quits(chat, hostmask, args):
    if join_leave_msgs == False: return
    if chat == irc:
        relay_quits(matrix, hostmask, args, 0)
    elif chat == matrix:
        relay_quits(irc, hostmask, args, 1)


@miniirc.Handler('PRIVMSG', colon=False)
def handle_privmsgs(chat, hostmask, args):
    words = args[-1].split(" ")
    w = [x.lower() for x in words]
    channel = args[0]
    command = words[0].lower()
    if chat == irc:
        relay_msgs(matrix, hostmask, args, 0)
    else:
        relay_msgs(irc, hostmask, args, 1)

@miniirc.Handler('TOPIC', colon=False)
def handle_topic(chat, hostmask, args):
    words = args[-1].split(" ")
    w = [x.lower() for x in words]
    channel = args[0]
    command = words[0].lower()
    if chat == irc:
        relay_topic(matrix, hostmask, args, 0)
    else:
        relay_topic(irc, hostmask, args, 1)


@miniirc.Handler('MODE', colon=False)
def handle_mode(chat, hostmask, args):
    if chat == irc:
        relay_mode(matrix, hostmask, args, 0)
    elif chat == matrix:
        relay_mode(irc, hostmask, args, 1)

