# RelixBot

*An IRC-Matrix bridge.*

## Unmaintained!

For those who didn't notice, I'm not working on this anymore. It's inferior 
compared to other bridges, and basically just a relaybot. The last commit I 
pushed is *years* old, and I haven't been using it for years.

For those who still want a bridge, here are some other, superior bridges you 
can use:

* [Heisenbridge](https://github.com/hifi/heisenbridge), if you want to bridge 
  Matrix to IRC with IRC puppets on Matrix and a relaybot on IRC side.
* [Matterbridge](https://github.com/42wim/matterbridge), what I've been using 
  instead. It can bridge Matrix, IRC, XMPP, Discord, and many other protocols.
  As such, the `f_ridge` bridge bot I run is powered by matterbridge.

This bridge might still work for you, in which case good luck, but don't expect 
any fixes to bugs you might encounter. It's encouraged that you stop using this 
bridge and move to the other aforementionned bridges going forward.
